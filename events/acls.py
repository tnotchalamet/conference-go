import json
import requests
from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY


def get_location_pictures(city, state):
    url = "https://api.pexels.com/v1/search"
    headers = {"Authorization": PEXELS_API_KEY}
    params = {"query": city + " " + state, "per_page": 1}
    response = requests.get(url, params=params, headers=headers)
    content = json.loads(response.content)
    return {"picture_url": content["photos"][0]["src"]["original"]}


def get_location_weather(city, state):
    geocodeurl = f"http://api.openweathermap.org/geo/1.0/direct?q={city, state}&appid={OPEN_WEATHER_API_KEY}"
    response = requests.get(geocodeurl)
    geo_data = json.loads(response.content)
    geolat = geo_data[0]["lat"]
    geolong = geo_data[0]["lon"]
    weather = f"https://api.openweathermap.org/data/2.5/weather?lat={geolat}&lon={geolong}&appid={OPEN_WEATHER_API_KEY}"
    response = requests.get(weather)
    current_weather = json.loads(response.content)
    return {
        "temp": current_weather["main"]["temp"],
        "description": current_weather["weather"][0]["description"],
    }
